require_relative 'Tokens'

class Lexer
	attr_reader :tokens, :errores, :salida
	
	def initialize input
		@tokens = []
		@salida = []
		@errores = []
		@fila = 0
		@columna = 0
		@input = input
	end

	#Recorre linea por linea el texto
	def recorrer
		@input.each_line do |line|
			#Se eliminan los espacios en blanco
			line.gsub!(/\t/, '  ')
			line.match(/\A\s*/)
			@columna = $&.length + 1
			line.sub!(/\A\s*/, '')
			line.sub!(/\n/,'')
			huboMatch = false
			@fila = @fila + 1
			#puts "Fila: #{@fila} #{line}"
			while line.length != 0
				huboMatch = false
				$tokens.each do |key, value|
					#Hay comentario
					if line.match(/\A#/)
						line.sub!(/\A.*/, '')
						huboMatch = true
						break
					else
						subString = line.match(value)
					end
					#Hay token
					if !$&.nil?
						#instanciar = Object::const_get(key)
						if key == :Salida || key == :Entero || key == :NumBits || key == :Identificador
							@salida << "#{key} at line #{@fila}, column #{@columna} with value '#{$&}'"
							#@tokens << [key, $&, @fila, @columna]
							@tokens << [key, [$&, @fila, @columna]]
							#@tokens << [key, $&]

						else
							@salida << "#{key} at line #{@fila}, column #{@columna}"
							#@tokens << [key, $&, @fila, @columna]
							@tokens << [$&, [$&, @fila, @columna]]
							#@tokens << [$&, $&] 
						end
						@columna = @columna + $&.length						
						line.sub!(value, '')
						#Variable auxiliar para contar la cantidad de espacios en blanco
						aux = line
						aux.match(/\A\s*/)
						@columna = @columna + $&.length
						line.sub!(/\A\s*/, '')
						huboMatch = true
						
						break
					end
				end
				#Hay error
				if !huboMatch
					line.match(/\A./)
					@errores << "Error: Se encontro un caracter inesperado \"#{$&}\" en la linea #{@fila}, columna #{@columna}."
					@columna =  @columna + 1
					line.sub!(/\A./, '')
					huboMatch = false
				end
			end
		end
	end
end
