require_relative 'tabla'
$errores = []
class AST

	def print_ indentar = ""
		puts "#{indentar}#{self.class}:"

		attrs.each do |a|
			a.print_ indentar + "  " if a.respond_to? :print_
		end
	end

	def check(sym)
		attrs.each do |a|
			a.check(sym)
		end
	end

	def alcance indentar = ""
		attrs.each do |a|
			a.alcance indentar + "	" if a.respond_to? :alcance
		end
	end

	def run()
		attrs.each do |a|
			a.run
		end
	end

	def attrs
		instance_variables.map do |a|
			instance_variables_get a
		end
	end
end

class Bitiondo < AST
	def initialize(b)
		@bloque = b
	end

	def print_ indentar = ""
		puts "#{indentar}#{self.class}"
		@bloque.print_
	end

	def check(sym=nil)
		@bloque.check(sym)
	end

	def alcance(indentar = "")
		@bloque.alcance()
	end

	def run()
		@bloque.run()
	end
end

class Bloque < AST
	attr_reader :tabla
	def initialize(d, c)
		@declaraciones = d
		@cuerpo = c
	end

	def print_ indentar = ""
		puts "#{indentar}#{self.class}"
		indentar += "  "
		
		#impresion de la lista de declaraciones
		puts "#{indentar}Begin"
		if @declaraciones != nil
			@declaraciones.each do |a|
				a.print_ (indentar)
			end
		end
		#impresion de la lista de instrucciones
		if @cuerpo != nil
			@cuerpo.each do |a|
				a.print_ (indentar)
			end
		end
		
		puts "#{indentar}End"
	end

	def check(sym)
		symh = SymTable.new(sym)
		#puts "Nuevo bloque:"
		if @declaraciones != nil
			@declaraciones.each do |de|
				de.check(symh)
			end
			
		end

		if @cuerpo != nil
			@cuerpo.each do |cu|
				cu.check(symh)
			end
		end
		@tabla = symh
	end

	def alcance(indentar = "")
		indentar += "  "
		puts "#{indentar}Begin"
		
		puts "#{indentar}  SymTable"
		if @declaraciones != nil
			@declaraciones.each do |a|
				a.alcance (indentar + "  ")
			end
		end
		#impresion de la lista de instrucciones
		if @cuerpo != nil
			@cuerpo.each do |a|
				a.alcance (indentar + "  ")
			end
		end
		
		puts "#{indentar}End"
	end

	def run()
		if @declaraciones != nil
			@declaraciones.each do |dec|
				dec.run()
			end
		end

		if @cuerpo != nil
			@cuerpo.each do |cu|
				cu.run()
			end
		end
	end

end

class Declaracion < AST
	attr_reader :tabla
	def initialize(t, l, v, tam)
		@tipo = t
		@ident = l
		@val = v
		@tamano = tam
	end

	def print_ indentar = ""
		puts "#{indentar}#{self.class}:"
		indentar += "  "
		
		@tipo.print_ (indentar)
		#print "#{indentar}Nombre: "
		#puts(@ident)
		@ident.print_ (indentar) 
		if !@val.nil?
			puts "#{indentar}Valor: "
			#puts(@val)
			@val.print_ (indentar)
		end
		if !@tamano.nil?
			puts "#{indentar}Tamano: "
			#puts(@val)
			@tamano.print_ (indentar)
		end 
	end

	def check(sym)
		nombre = @ident.check2(sym)
		tipo = @tipo.check(sym)
		if !@val.nil?
			aux1 = @val.check(sym)
		else
			aux1 = nil
		end

		if !@tamano.nil?
			aux2 = @tamano.check(sym)
		else
			aux2 = nil
		end

		#puts "#{nombre}, #{tipo}, #{aux1}, #{aux2}"

		if sym.isMemberHere(nombre)
			$errores << "Error linea #{@ident.fila}, columna #{@ident.columna}: Variable '#{nombre}' ya declarada"
		elsif (aux1 != nil && tipo != @val.tipo)
			$errores << "Error linea #{@val.fila}, columna #{@val.columna}: La variable '#{nombre}' es #{tipo}, y se le esta asignando un valor #{@val.tipo}"
		elsif (@tamano != nil && @tamano.tipo != "int")
			$errores << "Error linea #{@tamano.fila}, #{@tamano.columna}El tamano debe ser int"
		else
			sym.insert(nombre, tipo, aux1, aux2)
			#puts "#{sym.find(nombre)}"
		end
		#puts "#{sym.isMemberHere("t")}"
		@tabla = sym
	end

	def alcance(indentar="")
		indentar += "  "
		print "#{indentar}Name: "
		@ident.alcance(indentar)
		print ", Type: "
		@tipo.alcance(indentar)
		if !@val.nil?
			print ", Value: "
			@val.alcance
		end
		if !@tamano.nil?
			print ", Size: "
			@tamano.alcance
		end
		puts ""
	end

	def run()
		variable = @ident.valor
		@val.run
		info = @tabla.find(variable)
		if @val.nil?
			@tabla.update(variable, info[:tipo], nil, info[:tamano])
		else
			if @val.class != NBits
				@tabla.update(variable, info[:tipo], @val.valor, info[:tamano])
			else
				if (@val.valor.size() -2) == info[:tamano]
					@tabla.update(variable, info[:tipo], @val.valor, info[:tamano])
				else
					puts "Error: El tamano del numero no corresponde con el declarado"
				end
				
			end
		end
		
		puts "#{@tabla.find(variable)}"
	end
end

class Asignacion < AST
	attr_reader :ident, :val, :tabla
	def initialize(i, v)
		@ident = i
		@val = v
	end

	def print_ indentar = ""
		indentar += "  "
		puts "#{indentar}#{self.class}"
		indentar += "  "
		puts "#{indentar}Lado Izquierdo: "
		#puts "  #{indentar}#{@ident}"
		@ident.print_ indentar + "  "
		puts "#{indentar}Lado Derecho: "
		#puts "  #{indentar}#{@val}"
		@val.print_ indentar + "  "
	end

	def check(sym)
		nombre = @ident.check(sym)
		if !sym.isMember(nombre)
			$errores << "Error fila #{@ident.fila}, columa #{@ident.columna}: La variable '#{nombre}' no ha sido inicializada"
		else
			@val.check(sym)
			if @ident.tipo != @val.tipo
				$errores << "Error linea #{@val.fila}, columna #{@val.columna}: Asignacion de un valor '#{@val.tipo}' a una variable de tipo '#{@ident.tipo}'"
			end
		end		
		@tabla = sym
	end

	def alcance(indentar = "")
		indentar += "  "
		puts "#{indentar}#{self.class}"
		indentar += "  "
		print "#{indentar}Lado Izquierdo: "
		#puts "  #{indentar}#{@ident}"
		@ident.alcance indentar + "  "
		puts ""
		puts "#{indentar}Lado Derecho: "
		#puts "  #{indentar}#{@val}"
		@val.alcance indentar + "  "
		puts ""
	end

	def run()
		#Lado izquierdo
		variable = @tabla.find(@ident.valor)
		##nuevoVAlor = @val.valor
		nuevoVAlor = @val.run()
		@tabla.update(variable[:nombre], variable[:tipo], nuevoVAlor, variable[:tamano])
		puts "#{@tabla.find(@ident.valor)}"
	end
end

class AsignacionBits < AST
	attr_reader :ident, :val, :po
	def initialize(i, po, v)
		@ident = i
		@posicion = po
		@val = v
	end

	def print_ indentar = ""
		indentar += "  "
		puts "#{indentar}#{self.class}"
		indentar += "  "
		puts "#{indentar}Lado Izquierdo: "
		#puts "  #{indentar}#{@ident}"
		@ident.print_ indentar + "  "
		puts "#{indentar}Posicion: "
		@posicion.print_ indentar + "  "
		indentar += "  "
		puts "#{indentar}Lado Derecho: "
		#puts "  #{indentar}#{@val}"
		@val.print_ indentar + "  "
	end

	def check(sym)
		nombre = @ident.check(sym)
		if !sym.isMember(nombre)
			$errores << "Error fila #{@ident.fila}, columa #{@ident.columna}: La variable '#{nombre}' no ha sido inicializada"
		else
			num = @val.check(sym)
			##puts "#{@val.class}"
			if @val.class == Numero
				if num != "0" && num != "1"
					$errores << "Error linea #{@val.fila}, columna #{@val.columna}: Se esperaba '0' o '1'"					
				end
			end
		end		
		
	end

	def alcance(indentar = "")
		indentar += "  "
		puts "#{indentar}#{self.class}"
		indentar += "  "
		print "#{indentar}Lado Izquierdo: "
		#puts "  #{indentar}#{@ident}"
		@ident.alcance indentar + "  "
		puts ""
		print "#{indentar}Posicion: "
		@posicion.alcance indentar + "  "
		puts ""
		if @val.class == Numero
			print "#{indentar}Lado Derecho: "
			#puts "  #{indentar}#{@val}"
			@val.alcance indentar + "  "
			puts ""
		else
			puts "#{indentar}Lado Derecho: "
			#puts "  #{indentar}#{@val}"
			@val.alcance indentar + "  "
			puts ""	
		end
		
	end
end

class Condicional < AST
	def initialize(e, lc, le)
		@exp = e
		@cuerpo = lc
		@else = le
	end

	def print_ indentar = ""
		puts "#{indentar}#{self.class}: "
		indentar += "  "
		puts "#{indentar}If: "
		indentar += "  "
		puts "#{indentar}Expresion: "
		@exp.print_ (indentar)
		puts "#{indentar}Cuerpo: "
		@cuerpo.print_ (indentar)
		if !@else.nil?
			puts "#{indentar}Else: "
			@else.print_ (indentar)
		end

	end

	def check(sym)
		#puts "If: "
		tipoExp = @exp.check(sym)
		if (@exp.tipo != "bool")
			$errores << "Error linea #{@exp.fila}, columna #{@exp.columna}: Se esperaba un valor 'bool' en vez de '#{@exp.tipo}'"
		end
		#puts "#{sym}"
	end

	def alcance(indentar = "")
		puts "#{indentar}If"
		print "#{indentar}  condition: "
		@exp.alcance(indentar + "  ")
		puts ""
		puts "#{indentar}  instruction: "
		@cuerpo.alcance(indentar + "  ")
		if !@else.nil?
			puts "#{indentar}Else"
			@else.alcance(indentar + "  ")
		end
	end
end

class For < AST
	def initialize(i, n, exp, pa, ins)
		@ident = i
		@valor = n
		@expresion = exp
		@paso = pa
		@instrucciones = ins
	end

	def print_ indentar=""
		puts "#{indentar}Ciclo For:"
		indentar += "  "
		puts "#{indentar}Contador: "
		@ident.print_ (indentar)
		puts "#{indentar}Valor inicial: "
		@valor.print_ (indentar)
		puts "#{indentar}Expresion: "
		@expresion.print_ (indentar)
		puts "#{indentar}Paso: "
		@paso.print_ (indentar)
		puts "#{indentar}Instrucciones: "
		@instrucciones.print_ (indentar)
	end

	def check(sym)
		#puts "For: "
		symh = SymTable.new(sym)
		nombre = @ident.check(sym)
		valor = @valor.check(sym)

		if (!sym.isMember(nombre))
			symh.insert(nombre, "int", valor, nil)
			expresion = @expresion.check(symh)
			if (@expresion.tipo != "bool")
				$errores << "Error linea #{@expresion.fila}, columna #{@expresion.columna}: se esperaba un valor 'bool' en vez de '#{@expresion.tipo}'"
			end
			paso = @paso.check(sym)
		else
			$errores << "Error linea #{@ident.fila}, columna #{@ident.columna}: Variable '#{nombre}' ya declarada"	
		end
		
		#puts "#{symh.find(nombre)}"
		@instrucciones.check(symh)
	end

	def alcance(indentar="")
		puts "#{indentar}For"
		print "#{indentar}  variable: "
		@ident.alcance(indentar + "  ")
		puts ""
		print "#{indentar}  value: "
		@valor.alcance(indentar + "  ")
		puts ""
		puts "#{indentar}  condition: "
		@expresion.alcance(indentar + " ")
		print "#{indentar}  paso: "
		@paso.alcance(indentar + "  ")
		puts ""
		puts "#{indentar}  instruction: "

		indentar += "  "
		puts "#{indentar}  SymTable"
		
		print "  #{indentar}  Name: "
		@ident.alcance(indentar)
		print ", Type: "
		print "int"
		#@tipo.alcance(indentar)
		print ", Value: "
		@valor.alcance(indentar)
		puts ""
		@instrucciones.alcance(indentar)
	end
end

class ForBits < AST
	def initialize(expb, i, expint, hl, inst)
		@expresionb = expb
		@ident = i
		@expresionint = expint
		@hilo = hl
		@instrucciones = inst
	end

	def print_ indentar=""
		puts "#{indentar}Ciclo ForBits"
		indentar += "  "
		puts "#{indentar}Expresion Bits: "
		@expresionb.print_ (indentar)
		puts "#{indentar} Identificador: "
		@ident.print_ (indentar)
		puts "#{indentar}Expresion Int: "
		@expresionint.print_ (indentar)
		print "#{indentar} Going: "
		puts "#{@hilo[0]}"
		puts "#{indentar}Instrucciones: "
		@instrucciones.print_ (indentar)
	end

	def check(sym)
		symh = SymTable.new(sym)
		#puts "Forbits:"
		expbits = @expresionb.check(sym)
		nombre = @ident.check(sym)

		if (sym.isMember(expbits))
			if (!sym.isMember(nombre))
				symh.insert(nombre, "int", nil, nil)
			else
				$errores << "Error linea #{@ident.fila}, columna #{@ident.columna}: variable #{nombre} ya declarada"
			end
		else
			$errores << "Error linea #{@expresionb.fila}, columna #{@expresionb.columna}: Variable '#{expbits}' no ha sido declarada"
		end
		
		#puts "#{symh.find(nombre)}"
		@instrucciones.check(symh)
	end

	def alcance(indentar="")
		puts "#{indentar}Forbits"
		print "#{indentar}  bits: "
		@expresionb.alcance(indentar + "  ")
		puts ""
		print "#{indentar}  iteration: "
		@ident.alcance(indentar + "  ")
		puts ""
		print "#{indentar}  from: "
		@expresionint.alcance(indentar + "  ")
		puts ""
		print "#{indentar}  going: "
		puts "#{@hilo[0]}"

		#SymTable
		indentar += "  "
		puts "#{indentar}  SymTable"
		print "  #{indentar}  Name: "
		@ident.alcance(indentar)
		print ", Type: "
		print "int"
		#@tipo.alcance(indentar)
		puts ""
		@instrucciones.alcance(indentar)


	end
end

class CicloRepeat < AST
	def initialize(r, w, d)
		@repeat = r
		@while = w
		@do = d
	end

	def print_ indentar=""
		puts "#{indentar}Ciclo Repeat: "
		indentar += "  "
		puts "#{indentar}Repeat: "
		@repeat.print_ (indentar)
		puts "#{indentar}While: "
		@while.print_ (indentar)
		if !@do.nil?
			puts "#{indentar}Do: "
			@do.print_ (indentar)	
		end
		
	end

	def check(sym)
		#puts "Ciclo repeat:"
		@repeat.check(sym)
		expb = @while.check(sym)
		if (@while.tipo != "bool")
			$errores << "Error linea #{@while.fila}, columna #{@while.columna}: se esperaba expresion booleana en vez de #{@while.tipo}"
		end
		if !@do.nil?
			@do.check(sym)
		end
		#puts "#{sym}"
	end

	def alcance(indentar="")
		puts "#{indentar}CicloRepeat:"
		@repeat.alcance(indentar + "  ")
		puts "#{indentar}  while:"
		@while.alcance(indentar + "  ")
		if !@do.nil?
			puts "#{indentar}  Do: "
			@do.alcance(indentar + "  ")	
		end
	end
end

class CicloWhile < AST
	def initialize(exp, inst)
		@expresion = exp
		@instrucciones = inst
	end

	def print_ indentar=""
		puts "#{indentar}Ciclo While:"
		indentar += "  "
		puts "#{indentar}Expresion: "
		@expresion.print_ (indentar)
		puts "#{indentar}Instrucciones: "
		@instrucciones.print_ (indentar)
	end

	def check(sym)
		#puts "While:"
		expb = @expresion.check(sym)
		if (@expresion.tipo != "bool")
			$errores << "Error linea #{@expresion.fila}, columna #{@expresion.columna}: se esperaba expresion booleana en ves de #{@expresion.tipo}"
		end
		@instrucciones.check(sym)
	end

	def alcance(indentar="")
		puts "#{indentar}CicloWhile"
		@expresion.alcance(indentar + "  ")
		puts "#{indentar}  Do:"
		@instrucciones.alcance(indentar + "  ")
	end
end

class EntradaSalida < AST
	def initialize(m, l)
		@modo = m
		@lista = l
		
	end

	def print_ indentar=""
		if @modo[0] == 'input'
			puts "#{indentar}Input: "
			indentar += "  "
			#puts "#{indentar}Variable a usar: "
			@lista.print_ (indentar)
		elsif @modo[0] == 'output' 
			puts "#{indentar}Output: "
			indentar += "  "
			@lista.each do |eoi|
				eoi.print_ (indentar)
			end
		else
			puts "#{indentar}Outputln: "
			indentar += "  "
			@lista.each do |eoi|
				eoi.print_ (indentar)
			end
		end
	end

	def alcance(indentar="")
		if @modo[0] == 'input'
			print "#{indentar}Input: "
			indentar += "  "
			#puts "#{indentar}Variable a usar: "
			@lista.alcance (indentar)
			puts ""
		elsif @modo[0] == 'output' 
			print "#{indentar}Output: "
			indentar += "  "
			@lista.each do |eoi|
				eoi.alcance (indentar)
				print " "
			end
			puts ""
		else
			print "#{indentar}Outputln: "
			indentar += "  "
			@lista.each do |eoi|
				eoi.alcance (indentar)
				print " "
			end
			puts ""
		end
	end

	def check(sym)
		#puts "IO:"
		if @modo[0] == 'input'
			entrada = @lista.check(sym)
			if (sym.isMember(entrada))
				#puts "#{sym.find(entrada)}"
			else
				$errores << "Error linea #{@lista.fila}, columna #{@lista.columna}: variable '#{entrada}' no ha sido declarada"
			end
		else
			@lista.each do |li|
				salida = li.check(sym)
				if (li.tipo != "string")
					if (sym.isMember(salida))
						#puts "#{sym.find(salida)}"
					else
						$errores << "Error linea #{li.fila}, columna #{li.columna}: variable '#{salida}' no ha sido declarada"
					end
				else
					#puts "#{salida}"
				end
			end	
		end
	end

end

class Cadena < AST
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	def initialize(c)
		@cadena = c
	end

	def print_ indentar=""
		indentar += "  "
		puts "#{indentar}String: #{@cadena[0]}"
	end

	def check(sym)
		@tipo = "string"
		@fila = @cadena[1]
		@columna = @cadena[2]
		return @cadena[0]
	end
	def alcance(indentar="")
		print "#{@cadena[0]}"
	end
end

class Tipo < AST
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	def initialize(t)
		@tipo = t
	end

	def print_ indentar=""
		print "#{indentar}Tipo: "
		puts("#{@tipo[0]}")

	end
	def check(sym)
		@fila = @tipo[1]
		@columna = @tipo[2]
		return @tipo[0]
	end
	def alcance(indentar= "")
		print "#{@tipo[0]}"
	end
end

class False < AST
	attr_reader :ident
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def initialize(i)
		@ident = i
	end

	def print_ indentar=""
		indentar += "  "
		print "#{indentar}Booleano: "
		puts "#{ident[0]}"
	end

	def check(sym)
		@valor = @ident[0]
		@tipo = "bool"
		@fila = @ident[1]
		@columna = @ident[2]
		return @ident[0]
	end

	def alcance(indentar="")
		print "#{@ident[0]}"
	end
	def run()
		@valor = false
	end
end

class True < AST
	attr_reader :ident
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def initialize(i)
		@ident = i
	end

	def print_ indentar=""
		indentar += "  "
		print "#{indentar}Booleano: "
		puts "#{ident[0]}"
	end

	def check(sym)
		@valor = @ident[0]
		@tipo = "bool"
		@fila = @ident[1]
		@columna = @ident[2]
		return @ident[0]
	end

	def alcance(indentar="")
		print "#{@ident[0]}"
	end
	def run()
		@valor = true
	end
end

class Variable3 < AST
	attr_reader :ident
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	attr_reader :tabla
	def initialize(i)
		@ident = i
	end
	def print_ indentar = ""
		#puts "#{indentar}Variable: "
		#indentar += "  "
		puts "#{indentar}Variable: '#{@ident[0]}'"
	end
	def check(sym)
		@tabla = sym
		if sym.isMember(@ident[0])
			#puts "#{sym.find(@ident[0])[:tipo].class}"
			@tipo = sym.find(@ident[0])[:tipo]
			#puts "#{@tipo}"
		else
			#$errores << "Error linea #{@ident[1]}, columna #{@ident[2]}: variable '#{@ident[0]}' no ha sido declarada" 
		end
		@valor = @ident[0]
		@fila = @ident[1]
		@columna = @ident[2]
		return @ident[0]
	end
	def check2(sym)
		@tabla = sym
		if sym.isMemberHere(@ident[0])
			#puts "#{sym.find(@ident[0])[:tipo].class}"
			@tipo = sym.findHere(@ident[0])[:tipo]
			#puts "#{@tipo}"
		end
		@valor = @ident[0]
		@fila = @ident[1]
		@columna = @ident[2]
		return @ident[0]
	end
	def alcance(indentar = "")
		print "#{@ident[0]}"
	end
	def run()
		@valor = @tabla.find(@ident[0])[:valor]
	end
end

class Numero < AST
	attr_reader :d
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def initialize (d)
		@d = d
	end
	
	def print_ indentar = ""
		indentar += "  "
		puts "#{indentar}Numero Entero: '#{@d[0]}'"
	end
	def check(sym)
		@tipo = "int"
		@valor = @d[0].to_i
		@fila = @d[1]
		@columna = @d[2]
		return @d[0].to_i
	end
	def alcance(indentar="")
		print "#{@d[0]}"
	end
	def run()
		@valor = @d[0].to_i
	end
end

class NBits < AST
	attr_reader :ident
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def initialize(nb)
		@ident = nb
	end

	def print_ indentar = ""
		indentar += "  "
		puts "#{indentar}Numero en bits: #{@ident[0]}"
	end

	def check(sym)
		@valor = @ident[0]
		@tipo = "bits"
		@fila = @ident[1]
		@columna = @ident[2]
		return @ident[0]
	end
	def alcance(indentar = "")
		print "#{@ident[0]}"
	end
	def run()
		@valor = @ident[0]
	end
end

class OpUnaria < AST
	def initialize(op)
		@op = op
	end

	def print_ indentar= ""
		indentar += "  "
		puts "#{indentar}#{self.class}: "
		indentar += "  "
		puts "#{indentar} Operando: "
		@op.print_ (indentar)
	end

	def alcance indentar= ""
		indentar += "  "
		puts "#{indentar}#{self.class}: "
		indentar += "  "
		puts "#{indentar} Operando: "
		@op.alcance (indentar)
	end
end

class OpBinaria < AST
	def initialize(izq, der)
		@left = izq
		@right = der
		@linea = 0
		@columna = 0
	end

	def print_ indentar = ""
		indentar += "  "
		puts "#{indentar}#{self.class}"
		indentar = indentar + "  "
		puts "#{indentar}Lado Izquierdo:"
		#puts "  #{indentar}#{@left}"
		@left.print_ (indentar)
		puts "#{indentar}Lado Derecho:"
		#puts "  #{indentar}#{@right}"
		@right.print_ (indentar)
	end

	def alcance(indentar="")
		indentar += "  "
		puts "#{indentar}#{self.class}"
		indentar = indentar + "  "
		print "#{indentar}Lado Izquierdo: "
		@left.alcance (indentar)
		puts ""
		print "#{indentar}Lado Derecho: "
		@right.alcance (indentar)
		puts ""
	end
end

class Suma < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	attr_reader :opder
	attr_reader :opizq
	def check(sym)
		numL = @left.check(sym)
		numR = @right.check(sym)

		if ((@left.tipo == "bool" || @right.tipo == "bool" ) || (@left.tipo != @right.tipo))
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion aritmetica invalida '#{@left.tipo} + #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "int"
		
		@opizq = @left
		@opder = @right
		
	end
	def run()
		@opder.run()
		@opizq.run()

		if @opder.valor != nil && @opizq != nil
			if @left.tipo == "bits" && @right.tipo == "bits"
				@valor = sumarBits(@opizq.valor, @opder.valor)
			else
				@valor = @opizq.valor + @opder.valor
			end
		end
					
	end
end
class Resta < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	attr_reader :opder
	attr_reader :opizq
	def check(sym)
		numL = @left.check(sym)
		numR = @right.check(sym)

		if ((@left.tipo == "bool" || @right.tipo == "bool" ) || (@left.tipo != @right.tipo))
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion aritmetica invalida '#{@left.tipo} - #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "int"
		@opizq = @left
		@opder = @right
		 
		#@valor = numL - numR
	end
	def run()
		@opder.run()
		@opizq.run()
			if @opder.valor != nil && @opizq.valor != nil
				if @left.tipo == "bits" && @right.tipo == "bits"
				#@valor = sumarBits(@opizq.valor, @opder.valor)
			else
				@valor = @opizq.valor - @opder.valor
			end
			end
	end
end
class Mult < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	attr_reader :opizq
	attr_reader :opder
	def check(sym)
		numL = @left.check(sym)
		numR = @right.check(sym)

		if ((@left.tipo == "bool" || @right.tipo == "bool" ) || (@left.tipo != @right.tipo))
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion aritmetica invalida '#{@left.tipo} * #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "int"
		@opizq = @left
		@opder = @right
		 
		@valor = numL * numR
	end
	def run()
		@opder.run()
		@opizq.run()
		if @opizq.valor != nil && @opder.valor != nil
			if @left.tipo == "bits" && @right.tipo == "bits"
				#@valor = sumarBits(@opizq.valor, @opder.valor)
			else
				@valor = @opizq.valor * @opder.valor
			end
		end		
	end
end
class DivEntera < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	attr_reader :opder
	attr_reader :opizq
	def check(sym)
		numL = @left.check(sym)
		numR = @right.check(sym)

		if ((@left.tipo == "bool" || @right.tipo == "bool" ) || (@left.tipo != @right.tipo))
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion aritmetica invalida '#{@left.tipo} / #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "int"
		@opizq = @left
		@opder = @right
		
		
	end
	def run()
		@opder.run()
		@opizq.run()
		if @left.tipo == "bits" && @right.tipo == "bits"
			#@valor = sumarBits(@opizq.valor, @opder.valor)
		else
			if @opder.valor != 0
				@valor = @opizq.valor / @opder.valor
			else
				puts "Error linea #{@right.fila}, columna #{@right.columna}: No se puede dividir entre 0"
			end
		end		
	end
end
class Residuo < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def check(sym)
		numL = @left.check(sym)
		numR = @right.check(sym)

		if ((@left.tipo == "bool" || @right.tipo == "bool" ) || (@left.tipo != @right.tipo))
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion aritmetica invalida '#{@left.tipo} % #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "int"
		
		@valor = numL % numR
	end
	def run
		@left.run()
		@right.run()
		if @left.valor != nil && @right.valor != nil
			if @left.tipo == "bits" && @right.tipo == "bits"
				#@valor = sumarBits(@opizq.valor, @opder.valor)
			else
				@valor = @opizq.valor * @opder.valor
			end
		end
		
	end
end
class Conjuncion < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	attr_reader :opder
	attr_reader :opizq
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "bool" && @right.tipo == "bool")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion booleana invalida '#{@left.tipo} && #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bool"


		@valor = eL && eR
	end
	def run()
		@left.run()
		@right.run()
		if @left.valor != nil && @right != nil
			@valor = @left.valor && @right.valor
		end
	end
end
class Disyuncion < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "bool" && @right.tipo == "bool")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion booleana invalida '#{@left.tipo} || #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bool"

		@valor = eL || eR
	end
	def run
		@left.run()
		@right.run()
		if @left.valor != nil && @left.valor != nil
			@valor = @left.valor || @right.valor
		end
	end
end
class DesplDerecha < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "bits" && @right.tipo == "int")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion invalida '#{@left.tipo} >> #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bits"

		return "#{eL} >> #{eR}"

	end
end
class DesplIzquierda < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "bits" && @right.tipo == "int")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion invalida '#{@left.tipo} << #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bits"

		return "#{eL} << #{eR}"

	end
end
class MayorIgual < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "int" && @right.tipo == "int")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion booleana invalida '#{@left.tipo} >= #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bool"

		@valor = eL >= eR
	end
	def run()
		@left.run()
		@right.run()
		if @left.valor != nil && @right.valor != nil
			@valor = @left.valor >= @right.valor
		end
	end
end
class MenorIgual < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "int" && @right.tipo == "int")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion booleana invalida '#{@left.tipo} <= #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bool"

		@valor = eL <= eR
	end
	def run()
		@left.run()
		@right.run()
		if @left.valor != nil && @right.valor != nil
			@valor = @left.valor <= @right.valor
		end
	end
end
class Mayor < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "int" && @right.tipo == "int")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion booleana invalida '#{@left.tipo} > #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bool"

		@valor = eL > eR

	end
	def run()
		@left.run()
		@right.run()
		if @left.valor != nil && @right.valor != nil
			@valor = @left.valor > @right.valor
		end
	end
end
class Menor < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "int" && @right.tipo == "int")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion booleana invalida '#{@left.tipo} < #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bool"

		@valor = eL < eR

	end
	def run()
		@left.run()
		@right.run()
		if @left.valor != nil && @right.valor != nil
			@valor = @left.valor < @right.valor
		end
	end
end
class Igual < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "int" && @right.tipo == "int")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion booleana invalida '#{@left.tipo} == #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bool"

		@valor = eL == eR

	end
	def run()
		@left.run()
		@right.run()
		if @left.valor != nil && @right.valor != nil
			@valor = @left.valor == @right.valor
		end
	end
end
class Desigual < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "int" && @right.tipo == "int")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion booleana invalida '#{@left.tipo} != #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bool"

		@valor = eL != eR

	end
	def run()
		@left.run()
		@right.run()
		if @left.valor != nil && @right.valor != nil
			@valor = @left.valor != @right.valor
		end
	end
end
class ConjuncionBit < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "bits" && @right.tipo == "bits")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion booleana invalida '#{@left.tipo} & #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bits"

		return "#{eL} & #{eR}"

	end
end
class DisyuncionBit < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "bits" && @right.tipo == "bits")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion booleana invalida '#{@left.tipo} | #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bits"

		return "#{eL} | #{eR}"

	end
end
class DisyuncionExclusiva < OpBinaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	def check(sym)
		eL = @left.check(sym)
		eR = @right.check(sym)

		unless (@left.tipo == "bits" && @right.tipo == "bits")
			$errores << "Error linea #{@left.fila}, columna #{@left.columna}: operacion booleana invalida '#{@left.tipo} ^ #{@right.tipo}'"
		end
		@fila = @left.fila
		@columna = @left.columna
		@tipo = "bits"

		return "#{eL} ^ #{eR}"

	end
end

class Negativo < OpUnaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	attr_reader :valor
	def check(sym)
		op = @op.check(sym)

		unless (@op.tipo == "int")
			$errores << "Error linea #{@op.fila}, columna #{@op.columna}: operacion aritmetica a un valor de tipo #{@op.tipo}"
		end
		@fila = @op.fila
		@columna = @op.columna
		@tipo = "int"
		@valor = -op
	end
	def run()
		@op.run
		if @op.valor != nil
			@valor = -@op.valor
		end
	end
end
class NegacionBooleana < OpUnaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	def check(sym)
		op = @op.check(sym)

		unless (@op.tipo == "bool")
			$errores << "Error linea #{@op.fila}, columna #{@op.columna}: operacion booleana a un valor de tipo #{@op.tipo}"
		end
		@fila = @op.fila
		@columna = @op.columna
		@tipo = "bool"
		@valor = !op
	end
	def run()
		@op.run
		if @op.valor != nil
			@valor = !@op.valor
		end
	end
end
class NegacionBits < OpUnaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	def check(sym)
		op = @op.check(sym)

		unless (@op.tipo == "bits")
			$errores << "Error linea #{@op.fila}, columna #{@op.columna}: operacion bits a un valor de tipo #{@op.tipo}"
		end
		@fila = @op.fila
		@columna = @op.columna
		@tipo = "bits"
		return "~#{op}"
	end
end
class AInt < OpUnaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	def check(sym)
		op = @op.check(sym)

		unless (@op.tipo == "bits")
			$errores << "Error linea #{@op.fila}, columna #{@op.columna}: operacion bits a un valor de tipo #{@op.tipo}"
		end
		@fila = @op.fila
		@columna = @op.columna
		@tipo = "int"
		return "$#{op}"
	end
end
class At < OpUnaria
	attr_reader :tipo
	attr_reader :fila
	attr_reader :columna
	def check(sym)
		op = @op.check(sym)

		unless (@op.tipo == "int")
			$errores << "Error linea #{@op.fila}, columna #{@op.columna}: operacion int a un valor de tipo #{@op.tipo}"
		end
		@fila = @op.fila
		@columna = @op.columna
		@tipo = "bits"
		return "@#{op}"
	end
end
