

$tokens = {

	#Palabras reservadas
	Begin: /\Abegin\b/,
	End:	/\Aend\b/,
	Int:	/\Aint\b/,
	Bool:	/\Abool\b/,
	Bits:	/\Abits\b/,
	True:	/\Atrue\b/,
	False: /\Afalse\b/,
	Input:	/\Ainput\b/,
	Output:	/\Aoutput\b/,
	Outputln:	/\Aoutputln\b/,
	If:	/\Aif\b/,
	Else:	/\Aelse\b/,
	For:	/\Afor\b/,
	Forbits:	/\Aforbits\b/,
	As:	/\Aas\b/,
	From:	/\Afrom\b/,
	Going:	/\Agoing\b/,
	Higher:	/\Ahigher\b/,
	Lower:	/\Alower\b/,
	Repeat:	/\Arepeat\b/,
	While:	/\Awhile\b/,
	Do:	/\Ado\b/,
	#Simbolos
	PuntoYComa: /\A;/,
	Coma: /\A,/,
	AbreParentesis: /\A\(/,
	CierraParentesis: /\A\)/,
	#Desplazamiento
	DesplDerecha: /\A>>/,
	DesplIzquierda: /\A<</,
	#A entero
	AInt: /\A\$/,
	#Operadores
	Suma: /\A\+/,
	Resta: /\A\-/,
	Multiplicacion: /\A\*/,
	Division: /\A\//,
	Residuo: /\A%/,
	AbreCorchete: /\A\[/,
	CierraCorchete: /\A\]/,
	Conjuncion: /\A&&/,
	Disyuncion: /\A\|\|/,
	MayorIgual: /\A>=/,
	MenorIgual: /\A<=/,
	Mayor: /\A>/,
	Menor: /\A</,
	Igual: /\A==/,
	Desigual: /\A!=/,
	Negacion: /\A!/,
	Asignacion: /\A=/,
	#Operadores para bits
	NegacionBit: /\A~/,
	ConjuncionBit: /\A&/,
	DisyuncionBit: /\A\|/,
	DisyuncionExclusiva: /\A\^/,
	#At
	At: /\A@/,
	#Salida
	Salida: /\A"([^"\\]*(\\(\\|n|"))*)*"/,
	#Numeros
	NumBits: /\A0b[01]+/,
	Entero: /\A[0-9]+/,
	#Identificador
	Identificador: /\A[a-zA-Z]\w*/
}
