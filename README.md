Andre Corcuera 12-10660

Para la implementacion del lexer se decidio crear un archivo 'Tokens.rb'
donde se encontraran la lista de todos los tokens necesarios para el
lenguaje Bitiondo. Un archivo 'Lexer.rb' donde se realizara el analisis
lexicografico del archivo '.bto' pasando de parametro linea por linea,
en el cual se iran removiendo de la linea, los tokens encontrados y
guardados en un arreglo ([tokenid, [tokenvalor, fila, columna]]), en caso de no encontrar, se agregara el caracter desconocido a un arreglo de Strings. Los archivos 'parse.y' contiene las reglas
sintacticas del lenguaje y 'ast.rb' que contiene el arbol sintactico abstracto.

Por otra parte, se cuenta con un archivo "operacionesBits.rb" para realizar las operaciones con bits 

Estado actual:
El proyecto actualmente es capaz de realizar un analisis sintanctico sobre el archivo '.bto'
En caso de encontrar un token se mostrara de la siguiente forma:
'Token' at line 'fila', column 'columna'
si es un entero, string, numbits o identificador:
'Token' at line 'fila', column 'column' with value 'valor'
En caso de error:
Error: se encontro un caracter inesperado "caracter" en la linea 'fila', columna 'columna'

Por otra parte, el proyecto es capaz de imprimir el arbol sintactico abstracto y reportar
el primer error que consiga como:
Error: se encontro un token inesperado 'token' en la linea 'fila', columna 'columna'

Y es capaz de verificar el contexto de cada declaracion e instruccion e imprimir un arbol
sintactico distinto, con la tabla de simbolos resultante. Sin embargo, no es capaz de ejecutar el
codigo escrito en el .bto, solo muestra una tabla de simbolos actualizada de valores
si se trata de asignacion o declaracion de enteros o booleano.

Nota: Para correr ./bitiondo archivo.bto se tuvo que activar la opcion 'Permitir ejecutar el archivo
como un programa', verificar si dicha opcion esta activa.

