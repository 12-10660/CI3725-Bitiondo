#!/usr/bin/env ruby

class BitiondoParser

	prechigh
		left '[' ']'
		right '!' '~' '$' '@' UMINUS
		left '*' '/' '%'
		left '+' '-'
		left '<<' '>>'
		left '<' '<=' '>' '>='
		left '==' '!='
		left '&'
		left '^'
		left '|'
		left '&&'
		left '||'
	preclow

start Bitiondo
rule

Bitiondo: Bloque 	{result = Bitiondo.new(val[0])}
;

	#Nuevo bloque
Bloque: 'begin' ListDeclaraciones ListInstrucciones 'end' {result = Bloque.new(val[1], val[2])}
| 'begin' ListDeclaraciones 'end' {result = Bloque.new(val[1], nil)}
| 'begin' 'end' {result = Bloque.new(nil, nil)}
;

	#Lista de instrucciones
ListInstrucciones: Instrucciones {result = [val[0]]}
| Instrucciones ListInstrucciones {result = [val[0]] + val[1]}
;

	#Lista de declaraciones
ListDeclaraciones: Declaracion {result = [val[0]]}
| Declaracion ListDeclaraciones	{result = [val[0]] + val[1]}
;

	#Declaracion   (tipo, nombre, valor, tamano)
Declaracion: Tipo Identificador ';' {result = Declaracion.new(val[0], Variable3.new(val[1]),nil, nil)}
| Tipo Identificador '=' Expresion ';'	{result = Declaracion.new(val[0], Variable3.new(val[1]), val[3], nil)}
| Tipo Identificador '[' Entero ']' ';'	{result = Declaracion.new(val[0], Variable3.new(val[1]), nil, Numero.new(val[3]))}
| Tipo Identificador '[' Entero ']' '=' Expresion ';' {result = Declaracion.new(val[0], Variable3.new(val[1]), val[6], Numero.new(val[3]))}
;


	#Tipo de datos
Tipo: 'int'	{result = Tipo.new(val[0])}
| 'bool'	{result = Tipo.new(val[0])}
| 'bits'	{result = Tipo.new(val[0])}
;

	#Instrucciones
Instrucciones: Condicional 
| 'for' '(' Identificador '=' Entero ';' Expresion ';' Entero ')' Instrucciones {result = For.new(Variable3.new(val[2]), Numero.new(val[4]), val[6], Numero.new(val[8]), val[10])}
| 'forbits' Expresion 'as' Identificador 'from' Expresion 'going' HiLO Instrucciones {result = ForBits.new(val[1], Variable3.new(val[3]), val[5], val[7], val[8])}
| 'repeat' Instrucciones 'while' Expresion ';' {result = CicloRepeat.new(val[1], val[3], nil)}
| 'repeat' Instrucciones 'while' Expresion 'do' Instrucciones {result = CicloRepeat.new(val[1], val[3], val[5])}
| 'while' Expresion 'do' Instrucciones {result = CicloWhile.new(val[1], val[3])}
| Identificador '=' Expresion ';'	{result = Asignacion.new(Variable3.new(val[0]), val[2])}
| Identificador '[' Entero ']' '=' Expresion ';' {result = AsignacionBits.new(Variable3.new(val[0]), Numero.new(val[2]), val[5])}
| IO 
| Bloque
;

	#higher o lower
HiLO: 'higher'
| 'lower'
;

	#Condicional
Condicional: 'if' Expresion Instrucciones {result = Condicional.new(val[1], val[2], nil)}
| 'if' Expresion Instrucciones 'else' Instrucciones {result = Condicional.new(val[1], val[2], val[4])}
;

	#Entrada y salida
IO: 'input' Identificador ';' {result = EntradaSalida.new(val[0], Variable3.new(val[1]))}
| 'output' ListSalida ';' {result = EntradaSalida.new(val[0], val[1])}
| 'outputln' ListSalida ';' {result = EntradaSalida.new(val[0], val[1])}
;

	#Lista de strings de salida
ListSalida: Out {result = [val[0]]}
| Out ',' ListSalida {result = [val[0]] + val[2]}
;
	#Variable o string de salida
Out: Identificador {result = Variable3.new(val[0])}
| Salida {result = Cadena.new(val[0])}
;

	#Expresion aritmeticas y booleanas
Expresion: Identificador	{result = Variable3.new(val[0])}
| Entero	{result = Numero.new(val[0])}
| NumBits	{result = NBits.new(val[0])}
| 'true'	{result = True.new(val[0])}
| 'false'	{result = False.new(val[0])}
| Expresion '+' Expresion	{result = Suma.new(val[0], val[2])}
| Expresion '-' Expresion	{result = Resta.new(val[0], val[2])}
| Expresion '*' Expresion	{result = Mult.new(val[0], val[2])}
| Expresion '/' Expresion	{result = DivEntera.new(val[0], val[2])}
| Expresion '%' Expresion	{result = Residuo.new(val[0], val[2])}
| Expresion '&&' Expresion	{result = Conjuncion.new(val[0], val[2])}
| Expresion '||' Expresion	{result = Disyuncion.new(val[0], val[2])}
| Expresion '>=' Expresion	{result = MayorIgual.new(val[0], val[2])}
| Expresion '<=' Expresion	{result = MenorIgual.new(val[0], val[2])}
| Expresion '==' Expresion	{result = Igual.new(val[0], val[2])}
| Expresion '!=' Expresion	{result = Desigual.new(val[0], val[2])}
| Expresion '>' Expresion	{result = Mayor.new(val[0], val[2])}
| Expresion '<' Expresion	{result = Menor.new(val[0], val[2])}
| Expresion '&' Expresion	{result = ConjuncionBit.new(val[0], val[2])}
| Expresion '|' Expresion	{result = DisyuncionBit.new(val[0], val[2])}
| Expresion '>>' Expresion	{result = DesplIzquierda.new(val[0], val[2])}
| Expresion '<<' Expresion	{result = DesplDerecha.new(val[0], val[2])}
| Expresion '^' Expresion	{result = DisyuncionExclusiva.new(val[0], val[2])}
| '-' Expresion = UMINUS {result = Negativo.new(val[1])}
| '!' Expresion {result = NegacionBooleana.new(val[1])}
| '~' Expresion {result = NegacionBits.new(val[1])}
| '$' Expresion {result = AInt.new(val[1])}
| '@' Expresion {result = At.new(val[1])}
| '(' Expresion ')'	{result = val[1]}
;

---- header

require_relative 'ast'
require_relative 'errores'

---- inner

def on_error(id, token, stack)
	abort("Error: Caracter inesperado '#{token[0]}' en la linea: #{token[1]}, columna: #{token[2]}")
end

def next_token
	token = @tokens.shift

end

def parse(lista_tokens)

	@tokens = lista_tokens
	ast = do_parse
	return ast
end
