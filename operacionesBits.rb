def numeroBits(num)
	return num.sub!(/\A0b/,'')
end

def sumarBits(op1, op2)
	num1 = numeroBits(op1)
	num2 = numeroBits(op2)
	num1.reverse!
	num2.reverse!
	salida = ""
	carreo = false
	if num2.size == num1.size
		for i in 0..num1.size-1
			if (num1[i] == "1" && num2[i] == "0") || (num1[i] == "0" && num2[i] == "1")
				if carreo && i == (num1.size-1)
					puts "Error: el total de la suma sobrepasa el tamano del arrelgo"
					salida << "0"
				elsif carreo
					salida << "0"
				else
					salida << "1"
				end
			elsif num1[i] == "0" && num2[i] == "0"
				if carreo
					salida << "1"
				else
					salida << "0"
				end
				carreo = false
			elsif num1[i] == "1" && num2[i] == "1"
				salida << "0"
				carreo = true
				if carreo && i == (num1.size-1)
					puts "Error: el total de la suma sobrepasa el tamano del arrelgo"					
				end
			end
		end
	end
	return salida.reverse!
end

def desplazarDerecha(numbits, desplazar)
	numbits.reverse!
	numbits << "00"
	numbits.reverse!
	return numbits[0..(numbits.length-2)]
end

def desplazarIzquierda(numbits, desplazar)
	numbits << "00"
	return numbits
end

bits1 = "0b111"
bits2 = "0b001"
total = sumarBits(bits1, bits2)
puts "#{total}"

