##Tabla de simbolos

class SymTable
	attr_reader :tabla
	
	#Nueva tabla
	def initialize(padre = nil)
		@tabla = {}
		@padre = padre
	end

	#Insertar un elemento en la tabla
	def insert(nombre, tipo, valor, tamano)
		key = nombre.to_sym
		if tipo == "bits" 
			nuevo_elemento = {:nombre => nombre, :tipo => tipo, :valor => valor, :tamano => tamano}
			@tabla[key] =  nuevo_elemento
		else
			nuevo_elemento = {:nombre => nombre, :tipo => tipo, :valor => valor}
			@tabla[key] = nuevo_elemento
		end

	end

	#Elimina un elemento de la tabla
	def delete(nombre)
		key = nombre.to_sym
		@tabla.delete(key)
	end

	#Actualizar informacion de un elemento
	def update(nombre, tipo, valor, tamano)
		key = nombre.to_sym
		if @tabla.has_key?(key)
			if tipo == "bits"
				nuevo_elemento = {:nombre => nombre, :tipo => tipo, :valor => valor, :tamano => tamano}
				@tabla[key] =  nuevo_elemento
			else
				nuevo_elemento = {:nombre => nombre, :tipo => tipo, :valor => valor}
				@tabla[key] = nuevo_elemento
			end
		end
	end

	#Averigua si un elemento pertenece a la tabla
	def isMember(token)
		key = token.to_sym
		#return @tabla.has_key?(key)
		auxT = @tabla
		while (!auxT.nil?)
			if auxT.has_key?(key)
				return true
			else
				if @padre != nil
					return @padre.isMember(token)
					break
				else
					break
				end
				
			end
		end
		return false
	end

	def isMemberHere(token)
		key = token.to_sym
		return @tabla.has_key?(key)
	end

	#Retorna la informacion de un elemento
	def find(nombre)
		key = nombre.to_sym
		auxT = @tabla
		while !auxT.nil?
			if auxT.has_key?(key)
				return auxT[key]
			else
				if @padre != nil
					return @padre.find(nombre)
					break
				else
					break
				end
			end
		end
	end
	def findHere(nombre)
		key = nombre.to_sym
		if @tabla.has_key?(key)
			return @tabla[key]
		end
	end
end

=begin
tabla1 = SymTable.new()
tabla1.insert("x", "int", nil, nil)
tabla1.insert("y", "bool", nil, nil)
tabla2 = SymTable.new(tabla1)
tabla2.insert("z", "int", nil, nil)

puts "#{tabla2.isMember("z")}"
=end